.PHONY: all clean realclean deployment deploy-reg-server locales check

HOSTNAME_FQDN := codeberg-test.org

BUILDDIR := /tmp/build
GOROOT := ${BUILDDIR}/go
GOPATH := ${BUILDDIR}/reg-server
PATH := ${GOROOT}/bin:${GOPATH}/bin:${PATH}

GOTAR = go1.18.$(shell uname | tr [:upper:] [:lower:])-amd64.tar.gz

TARGETS = ${GOPATH}/bin/reg-server

all : ${TARGETS}

locales:
	find locales -name "*.po" -execdir msgfmt "{}" ";"

${GOPATH}/bin/reg-server :  main.go ${GOROOT}/bin/go locales
	go build -o $@ $<

${GOROOT}/bin/go :
	mkdir -p ${GOROOT}/Downloads
	wget -c --no-verbose --directory-prefix=${GOROOT}/Downloads https://dl.google.com/go/${GOTAR}
	tar xfz ${GOROOT}/Downloads/${GOTAR} -C ${BUILDDIR}

deployment : deploy-reg-server

deploy-reg-server : ${GOPATH}/bin/reg-server
	-ssh root@${HOSTNAME_FQDN} systemctl stop reg-server
	rsync -av -e ssh --chown=root:root $< root@${HOSTNAME_FQDN}:/usr/local/bin/
	rsync -av -e ssh --chown=root:root locales templates root@${HOSTNAME_FQDN}:/etc/reg-server/
	ssh root@${HOSTNAME_FQDN} systemctl daemon-reload
	ssh root@${HOSTNAME_FQDN} systemctl enable reg-server
	ssh root@${HOSTNAME_FQDN} systemctl start reg-server
	ssh root@${HOSTNAME_FQDN} systemctl status reg-server

clean :
	@rm -f ${TARGETS}

realclean :
	@chmod -R u+w ${GOPATH}
	@rm -rf ${GOPATH}
	@find locales -name "*.mo" -exec rm "{}" ";"

check : ${GOROOT}/bin/go
	go test -v

lint :
	go install mvdan.cc/gofumpt@latest
	gofumpt -extra -w .
