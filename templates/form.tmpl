<!DOCTYPE html>
<html lang="{{ PGettext "Language Identifier" "en" }}" class="codeberg-design">
<head>
	<!-- Meta tags -->
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
	<meta name="viewport" content="width=device-width" />

	<!-- Favicon and title -->
	<title>{{ PGettext "Page Title" "Join Codeberg e. V." }}</title>

	<!-- Codeberg Design -->
	<link rel="icon" href="https://design.codeberg.org/logo-kit/favicon.ico" type="image/x-icon" />
	<link rel="icon" href="https://design.codeberg.org/logo-kit/favicon.svg" type="image/svg+xml" />
	<link rel="apple-touch-icon" href="https://design.codeberg.org/logo-kit/apple-touch-icon.png" />

	<link rel="stylesheet" href="https://design.codeberg.org/design-kit/codeberg.css" />
	<script defer src="https://design.codeberg.org/design-kit/codeberg.js"></script>

	<link href="https://fonts.codeberg.org/dist/inter/Inter%20Web/inter.css" rel="stylesheet" />
	<link href="https://fonts.codeberg.org/dist/fontawesome5/css/all.min.css" rel="stylesheet" />

</head>
<body data-set-preferred-mode-onload="true">
	<div class="page-wrapper with-navbar">
		<codeberg-navbar></codeberg-navbar>
		<div class="content-wrapper">
			<div class="content">
				<h2 class="content-title">
					{{ Gettext "Become a Member" }}
				</h2>
				{{ Gettext "Become a Member (Markdown Description)" | Markdown }}
			</div>
			<form class="container-fluid" method="post" action="/post">
				<div class="row" style="margin-right: 30px;">
					<div class="col-xl">
						<div class="card mt-0 mr-0">
							<h2 class="card-title">
								{{ Gettext "Kind of Membership" }}
							</h2>
							{{ Gettext "Kind of Membership (Markdown Description)" | Markdown }}
							<hr class="my-15">
							<div class="form-group">
								<label for="member-type" class="required">{{ Gettext "Who or what are you?" }}</label>
								<select class="form-control" name="member-type" id="member-type" required="required">
									<option value="person" selected="selected">{{ Gettext "Private Individual" }}</option>
									<option value="company">{{ Gettext "Legal Entity (e.g. organization or company)" }}</option>
								</select>
							</div>
							<div class="form-group mb-0">
								<label for="membership-type" class="required">{{ Gettext "What kind of member do you want to become?" }}</label>
								<select class="form-control" name="membership-type" id="membership-type" required="required">
									<option value="active-member" selected="selected">{{ Gettext "Active Member" }}</option>
									<option value="supporting-member">{{ Gettext "Supporting Member" }}</option>
								</select>
							</div>
						</div>
						<div class="card mr-0">
							<h2 class="card-title">
								{{ Gettext "Volunteer Work" }}
							</h2>
							{{ Gettext "Volunteer Work (Markdown Description)" | Markdown }}
							<hr class="my-15">
							<div class="form-group mb-0">
								<label>{{ Gettext "I'd like to support you in the following areas:" }}</label>
								{{ range Options "application-development" "it-security" "database-engineering" "distributed-filesystems" "cluster-infrastructure" "bookkeeping-and-finances" "legal-stuff" "public-relations" "fundraising" }}
								<div class="custom-checkbox mb-5">
									<input type="checkbox" value="true" name="volunteer-work[{{ . }}]" id="volunteer-work[{{ . }}]">
									<label for="volunteer-work[{{ . }}]">{{ PGettext "Volunteer Work Options" . }}</label>
								</div>
								{{ end }}
								<div class="form-group mt-15 mb-0">
									<label for="volunteer-work[other]">{{ PGettext "Volunteer Work Options" "Other areas where I can help:" }}</label>
									<textarea class="form-control" name="volunteer-work[other]" id="volunteer-work[other]"></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl">
						<div class="card mt-0 mr-0">
							<h2 class="card-title">
								{{ Gettext "Personal Information" }}
							</h2>
							<div class="form-row row-eq-spacing-sm">
								<div class="col-sm">
									<label for="first-name" class="required">{{ Gettext "First Name:" }}</label>
									<input type="text" class="form-control" name="first-name" id="first-name" required="required">
								</div>
								<div class="col-sm">
									<label for="last-name" class="required">{{ Gettext "Last Name:" }}</label>
									<input type="text" class="form-control" name="last-name" id="last-name" required="required">
								</div>
							</div>
							<div class="form-group" hidden-if="val('member-type') == 'person'" hidden>
								<label for="company" class="required">{{ Gettext "Organization or Company:" }}</label>
								<input type="text" class="form-control" name="company-name" id="company-name" required-if="val('member-type') == 'company'">
							</div>
							{{ Gettext "Email Address (Markdown Description)" | Markdown }}
							<div class="form-group">
								<label for="email" class="required">{{ Gettext "Email Address:" }}</label>
								<input type="email" class="form-control" name="email-address" id="email-address" required="required">
							</div>
							<hr class="my-15">
							{{ Gettext "Postal Address (Markdown Description)" | Markdown }}
							<div class="form-group">
								<label for="street" class="required">{{ Gettext "Street Address:" }}</label>
								<input type="text" class="form-control" name="street-address" id="street-address" required="required">
							</div>
							<div class="form-group">
								<label for="street-detail">{{ Gettext "Address Details:" }}</label>
								<input type="text" class="form-control" name="address-detail" id="address-detail">
							</div>
							<div class="form-row row-eq-spacing-sm">
								<div class="col-sm-3">
									<label for="postal-code" class="required">{{ Gettext "Postal Code:" }}</label>
									<input type="tel" class="form-control" name="postal-code" id="postal-code" required="required">
								</div>
								<div class="col-sm-9">
									<label for="city" class="required">{{ Gettext "City:" }}</label>
									<input type="text" class="form-control" name="city" id="city" required="required">
								</div>
							</div>
							<div class="form-group mb-0">
								<label for="country" class="required">{{ Gettext "Country:" }}</label>
								<input type="text" class="form-control" name="country" id="country">
							</div>
							<hr class="my-15">
							{{ Gettext "Accept Privacy Policy (Markdown Description)" | Markdown }}
							<div class="custom-checkbox">
								<input type="checkbox" name="accept-privacy-policy" id="accept-privacy-policy" value="true" required="required">
								<label for="accept-privacy-policy" class="required">{{ Gettext "I accept the privacy policy" }}</label>
							</div>
						</div>
					</div>
					<div class="col-xl">
						<div class="card mt-0 mr-0">
							<h2 class="card-title">
								{{ Gettext "Membership Fee" }}
							</h2>
							{{ Gettext "Membership Fee Information (Markdown Description)" | Markdown }}
							<div hidden-if="val('member-type') == 'person'" hidden>
								{{ Gettext "Additional Membership Fee Information for Legal Entities (Markdown Description)" | Markdown }}
							</div>

							<div class="form-group custom-checkbox">
								<input type="checkbox" name="apply-for-honorary-membership" id="apply-for-honorary-membership" value="true">
								<label for="apply-for-honorary-membership">{{ Gettext "I want to apply for a discounted membership fee" }}</label>
							</div>

							<div hidden-if="!val('apply-for-honorary-membership')">
								{{ Gettext "Discounted Membership Information (Markdown Description)" | Markdown }}
								<div class="form-group">
									<label for="reason-for-honorary-membership">{{ Gettext "Why do you want to pay a discounted membership fee?" }}</label>
									<textarea class="form-control" name="reason-for-honorary-membership" id="reason-for-honorary-membership"></textarea>
								</div>
							</div>

							<div hidden-if="val('apply-for-honorary-membership')">
								<hr class="my-15">

								<div class="form-group mb-5">
									<label for="membership-fee" class="required">{{ Gettext "Chosen yearly membership fee:" }}</label>
									<div class="form-row row-eq-spacing-sm mb-10">
										<div class="col" style="padding-top: 1px" hidden hidden-if="false"><!-- only show with JavaScript enabled -->
											<input id="membership-fee-slider" type="range" value="48" step="2" min="10" max="240" oninput="document.getElementById('membership-fee').value = this.value">
										</div>
										<div class="col pr-0" style="max-width: 6.5em;">
											<div class="input-group">
												<input class="form-control" type="tel" name="membership-fee" id="membership-fee" value="48" required-if="!val('apply-for-honorary-membership')" onchange="document.getElementById('membership-fee-slider').value = this.value">
												<div class="input-group-append">
													<span class="input-group-text">€</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--<div class="card row mx-0 p-0">
									<div class="col-3 p-15 pr-0">
										<img src="https://cdn1.iconfinder.com/data/icons/filled-line-christmas-icons/75/_deer_smile-512.png" style="width: 100%;">
									</div>
									<div class="col-9 p-15">
										<h2 class="card-title mb-0">
											Du bist toll!
										</h2>
										<p class="mt-5 mb-0">
											Mit deinem Beitrag können sich unsere virtuellen Rentiere jede Woche einen neuen
											Witz für diese fühl-dich-gut-während-du-Geld-ausgibst-Box ausdenken!
										</p>
									</div>
								</div>-->
								<hr class="mb-15 mt-10">

								<div class="form-group">
									<label for="payment-method" class="required">{{ Gettext "Payment Method (SEPA transactions must be at least 10 €):" }}</label>
									<select class="form-control" name="payment-method" id="payment-method" required-if="!val('apply-for-honorary-membership')">
										<option value="sepa-yearly" selected="selected" content-from="`{{ PGettext "Payment Method" "SEPA Direct Debit (${fmt(val('membership-fee'), 0, 2)} € yearly)" }}`"></option>
										<option value="sepa-half-yearly" disabled-if="val('membership-fee') < 20" content-from="`{{ PGettext "Payment Method" "SEPA Direct Debit (${fmt(val('membership-fee') / 2, 0, 2)} € half-yearly)" }}`"></option>
										<option value="sepa-quarterly" disabled-if="val('membership-fee') < 40" content-from="`{{ PGettext "Payment Method" "SEPA Direct Debit (${fmt(val('membership-fee') / 4, 0, 2)} € quarterly)" }}`"></option>
										<option value="sepa-monthly" disabled-if="val('membership-fee') < 120" content-from="`{{ PGettext "Payment Method" "SEPA Direct Debit (${fmt(val('membership-fee') / 12, 0, 2)} € monthly)" }}`"></option>
										<option value="manual">{{ PGettext "Payment Method" "Manual Bank Transfer" }}</option>
									</select>
								</div>

								<div hidden-if="val('payment-method') !== 'manual'">
									{{ Gettext "Manual Bank Transfer Information (Markdown Description)" | Markdown }}

									<pre class="code">IBAN DE90 8306 5408 0004 1042 42<br>BIC  GENODEF1SLR</pre>
								</div>

								<div hidden-if="!val('payment-method').startsWith('sepa-')">
									<div class="form-group">
										<label for="iban" class="required">IBAN:</label>
										<input type="text" class="form-control" name="iban" id="iban" required-if="!val('apply-for-honorary-membership') && val('payment-method').startsWith('sepa-')">
									</div>
									<div class="form-group">
										<label for="bic" class="required">BIC:</label>
										<input type="text" class="form-control" name="bic" id="bic" required-if="!val('apply-for-honorary-membership') && val('payment-method').startsWith('sepa-')">
									</div>
									<div class="form-group custom-checkbox">
										<input type="checkbox" name="accept-sepa-conditions" id="accept-sepa-conditions" value="true" required-if="!val('apply-for-honorary-membership') && val('payment-method').startsWith('sepa-')">
										<label for="accept-sepa-conditions" class="required">{{ Gettext "SEPA Disclaimer (Markdown)" | Markdown }}</label>
									</div>
								</div>
							</div>
						</div>
						<div class="card mr-0">
							<h2 class="card-title">
								{{ Gettext "Submit Membership Application" }}
							</h2>
							<div class="custom-checkbox form-group">
								<input type="checkbox" name="accept-bylaws" id="accept-bylaws" value="yes" required="required">
								<label for="accept-bylaws" class="required">
									{{ Gettext "Accept Bylaws (Markdown Description)" | Markdown }}
								</label>
							</div>
							<button class="btn btn-primary btn-lg w-full" type="submit">{{ Gettext "Become Member Now!" }}</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script>
		function updateConditionalForms() {
			const val = id => (e => (e.getAttribute("type") === "checkbox" || e.getAttribute("type") === "radio") ? e.checked : e.value)(
					document.getElementById(id) ||
					[...document.getElementsByName(id)].filter(e => e.hasAttribute("checked"))[0] ||
					document.getElementsByName(id)[0]
			);
			const fmt = (num, digits, precision) => Number(num)
					.toFixed(precision)
					.replace(/^[^.]*/, x => x.padStart(digits, "0"));
			[...document.querySelectorAll("[hidden-if]")].forEach(e => {
				e.hidden = eval(e.getAttribute("hidden-if"));
			});
			[...document.querySelectorAll("[disabled-if]")].forEach(e => {
				e.disabled = eval(e.getAttribute("disabled-if"));
			});
			[...document.querySelectorAll("[required-if]")].forEach(e => {
				e.required = eval(e.getAttribute("required-if"));
			});
			[...document.querySelectorAll("[class-if]")].forEach(e => {
				const classConditions = JSON.parse(e.getAttribute("class-if"));
				for (const className of classConditions) {
					if (eval(classConditions[className])) {
						e.classList.add(className);
					} else {
						e.classList.remove(className);
					}
				}
			});
			[...document.querySelectorAll("[value-from]")].forEach(e => {
				e.value = eval(e.getAttribute("value-from"));
			});
			[...document.querySelectorAll("[content-from]")].forEach(e => {
				e.textContent = eval(e.getAttribute("content-from"));
			});
		}
		updateConditionalForms();
		document.addEventListener("input", () => updateConditionalForms());
		document.addEventListener("change", () => updateConditionalForms());
	</script>
</body>
</html>
